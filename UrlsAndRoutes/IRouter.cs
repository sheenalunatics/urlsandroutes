﻿using System.Threading.Tasks;

namespace Microsoft.AspNetCore.Routing
{
    public interface IRouteConstraint
    {
        Task RouteAsync(RouteContext context);
        VirtualPathData GetVirtualPath(VirtualPathContext context);
    }
}

namespace Microsoft.AspNetCore.Http
{
    public delegate Task RequestDelegate(HttpContext context);
}